/**
* This file is part of DSO.
* 
* Copyright 2016 Technical University of Munich and Intel.
* Developed by Jakob Engel <engelj at in dot tum dot de>,
* for more information see <http://vision.in.tum.de/dso>.
* If you use this code, please cite the respective publications as
* listed on the above website.
*
* DSO is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DSO is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DSO. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#include <iostream>
#include <fstream>
#include <ros/ros.h>
//check if eigen_conversion is being loaded
#include <eigen_conversions/eigen_msg.h>

#include "boost/thread.hpp"
#include "util/MinimalImage.h"
#include "IOWrapper/Output3DWrapper.h"

#include "dso_ros/keyframeMsg.h"
#include "dso_ros/hessianPoint.h"
//#include "dso_ros/graphMsg.h"
#include "dso_ros/poseMsg.h"

#include "FullSystem/HessianBlocks.h"
#include "util/FrameShell.h"

namespace dso
{

class FrameHessian;
class CalibHessian;
class FrameShell;

    namespace IOWrap
    {

        class SampleOutputWrapper : public Output3DWrapper
        {
        public:

            inline SampleOutputWrapper()
            {
                std::ofstream newfile;
                newfile.open("/home/marinho/points.txt");
                newfile << "";
                newfile.close();

                std::ofstream cmfile;
                cmfile.open("/home/marinho/camera.txt");
                cmfile << "";
                cmfile.close();

                std::ofstream cmposefile;
                cmposefile.open("/home/marinho/camera_pose.txt");
                cmposefile << "";
                cmposefile.close();
                //printf("OUT: Created RosOutputWrapper\n");
            }

            virtual ~SampleOutputWrapper()
            {
                //printf("OUT: Destroyed SampleOutputWrapper\n");
            }

            virtual void publishGraph(const std::map<uint64_t, Eigen::Vector2i, std::less<uint64_t>, Eigen::aligned_allocator<std::pair<const uint64_t, Eigen::Vector2i>>> &connectivity) override
            {
                /*
                //printf("OUT: got graph with %d edges\n", (int)connectivity.size());

                int maxWrite = 5;

                for (const std::pair<uint64_t, Eigen::Vector2i> &p : connectivity)
                {
                    int idHost = p.first >> 32;
                    int idTarget = p.first & ((uint64_t)0xFFFFFFFF);
                    //printf("OUT: Example Edge %d -> %d has %d active and %d marg residuals\n", idHost, idTarget, p.second[0], p.second[1]);
                    maxWrite--;
                    if (maxWrite == 0)
                        break;
                }
                */
            }

            virtual void publishKeyframes(std::vector<FrameHessian *> &frames, bool final, CalibHessian *HCalib) override
            {
                //////////////////////////////////////////////////////////////*
                // final = variable to check if is the end of this keyframe  //
                // use final to only great                                   //
                //////////////////////////////////////////////////////////////*/
                dso_ros::keyframeMsg fMsg;
                dso_ros::hessianPoint hPoint;
                std_msgs::Float64MultiArray camToWorld;

                std::ofstream myfile;
                std::ofstream camfile;
                myfile.open("/home/marinho/points.txt", std::ios::app);
                camfile.open("/home/marinho/camera.txt", std::ios::app);

                float fx, fy, cx, cy;
                float fxi, fyi, cxi, cyi;
                //float colorIntensity = 1.0f;

                fx = HCalib->fxl();
                fy = HCalib->fyl();
                cx = HCalib->cxl();
                cy = HCalib->cyl();
                fxi = 1 / fx;
                fyi = 1 / fy;
                cxi = -cx / fx;
                cyi = -cy / fy;

                fMsg.final = final;

                if (final)
                {
                    for (FrameHessian *f : frames)
                    {
                        if (f->shell->poseValid)
                        {

                            fMsg.timestamp = f->shell->timestamp;
                            fMsg.pointHessianSize = (int)f->pointHessians.size();
                            fMsg.pointHessiansMarginalizedSize = (int)f->pointHessiansMarginalized.size();
                            fMsg.immaturePointsSize = (int)f->immaturePoints.size();
                            camfile << f->shell->camToWorld.translation()[0] << " " << f->shell->camToWorld.translation()[1] << " " << f->shell->camToWorld.translation()[2] << "\n";
                            tf::matrixEigenToMsg(f->shell->camToWorld.matrix3x4(), camToWorld);
                            fMsg.camToWorld = camToWorld;

                            auto const &m = f->shell->camToWorld.matrix3x4();

                            // use only marginalized points.
                            auto const &points = f->pointHessiansMarginalized;

                            for (auto const *p : points)
                            {
                                float depth = 1.0f / p->idepth;
                                auto const x = (p->u * fxi + cxi) * depth;
                                auto const y = (p->v * fyi + cyi) * depth;
                                auto const z = depth * (1 + 2 * fxi);

                                Eigen::Vector4d camPoint(x, y, z, 1.f);
                                Eigen::Vector3d worldPoint = m * camPoint;

                                hPoint.x = (float)worldPoint[0];
                                hPoint.y = (float)worldPoint[1];
                                hPoint.z = (float)worldPoint[2];

                                hPoint.depthScaled = p->idepth_scaled;
                                hPoint.depthStdDev = sqrt(1.0f / p->idepth_hessian);
                                hPoint.inlierResiduals = p->numGoodResiduals;

                                fMsg.points.push_back(hPoint);

                                myfile << worldPoint[0] << " " << worldPoint[1] << " " << worldPoint[2] << "\n";
                                
                            }
                            keyframe_publisher.publish(fMsg);
                        }
                    }
                }

                /*
                for (FrameHessian *f : frames)
                {
                    //fMsg.frameId = f->frameID;
                    fMsg.incomingId = f->shell->incoming_id;
                    fMsg.timestamp = f->shell->timestamp;
                    fMsg.pointHessianSize = (int)f->pointHessians.size();
                    fMsg.pointHessiansMarginalizedSize = (int)f->pointHessiansMarginalized.size();
                    fMsg.immaturePointsSize = (int)f->immaturePoints.size();
                    tf::matrixEigenToMsg(f->shell->camToWorld.matrix3x4(), camToWorld);
                    fMsg.camToWorld = camToWorld;

                    for (PointHessian *p : f->pointHessians)
                    {
                        hPoint.x = p->u;
                        hPoint.y = p->v;
                        hPoint.depthScaled = p->idepth_scaled;
                        hPoint.depthStdDev = sqrt(1.0f / p->idepth_hessian);
                        hPoint.inlierResiduals = p->numGoodResiduals;

                        myfile << p->u << " " << p->v << " " << p->idepth_hessian << "\n";

                        fMsg.points.push_back(hPoint);
                    }

                    keyframe_publisher.publish(fMsg);
                }
                */

                myfile.close();
                camfile.close();
            }

            virtual void publishCamPose(FrameShell *frame, CalibHessian *HCalib) override
            {

                std::ofstream camposefile;
                camposefile.open("/home/marinho/camera_pose.txt", std::ios::app);
                dso_ros::poseMsg pMsg;

                pMsg.incoming_id = frame->incoming_id;
                pMsg.timestamp = frame->timestamp;
                pMsg.id = frame->id;

                camposefile << frame->camToWorld.matrix3x4() << "\n\n";
                std_msgs::Float64MultiArray camToWorld;
                tf::matrixEigenToMsg(frame->camToWorld.matrix3x4(), camToWorld);
                pMsg.camToWorld = camToWorld;

                pose_publisher.publish(pMsg);

                camposefile.close();
            }

            virtual void pushLiveFrame(FrameHessian *image) override
            {
                // can be used to get the raw image / intensity pyramid.
            }

            virtual void pushDepthImage(MinimalImageB3 *image) override
            {
                // can be used to get the raw image with depth overlay.
            }
            virtual bool needPushDepthImage() override
            {
                return false;
            }

            virtual void pushDepthImageFloat(MinimalImageF *image, FrameHessian *KF) override
            {
                
                /*
                printf("OUT: Predicted depth for KF %d (id %d, time %f, internal frame-ID %d). CameraToWorld:\n",
                    KF->frameID,
                    KF->shell->incoming_id,
                    KF->shell->timestamp,
                    KF->shell->id);
                std::cout << KF->shell->camToWorld.matrix3x4() << "\n";
                */
               /*
                int maxWrite = 5;
                for (int y = 0; y < image->h; y++)
                {
                    for (int x = 0; x < image->w; x++)
                    {
                        if (image->at(x, y) <= 0)
                            continue;

                        //printf("OUT: Example Idepth at pixel (%d,%d): %f.\n", x, y, image->at(x, y));
                        maxWrite--;
                        if (maxWrite == 0)
                            break;
                    }
                    if (maxWrite == 0)
                        break;
                }
                */
                
            }
            private:
                ros::NodeHandle nh_;
                ros::Publisher keyframe_publisher = nh_.advertise<dso_ros::keyframeMsg>("keyframes", 1);
                ros::Publisher pose_publisher = nh_.advertise<dso_ros::poseMsg>("pose", 1);
                //ros::Publisher graph_publisher = nh_.advertise<dso_ros::graphMsg>("graph", 1);
        };
    }
}